#!/bin/sh

  
if [ -f "addresses.txt" ] ; then
    echo "File exists!"
else  
    touch addresses.txt
fi

register(){
    echo "Enter first name:"
    read FNAME
    echo "Enter surname:"
    read LNAME
    echo "Enter address:"
    read ADDR
    echo "Enter phone:"
    read PHONE
    FILE_INPUT="$FNAME $LNAME $ADDR $PHONE"
    echo $FILE_INPUT
    echo "$FILE_INPUT" >> addresses.txt 
}

view(){
    SEARCH_CONTENT="`cat addresses.txt | grep $1`"
    if [ -z "$SEARCH_CONTENT" ]; then
        echo "No match found"
    else
        cat addresses.txt | grep $1
    fi
}

remove(){       
    SEARCH_CONTENT="`cat addresses.txt | grep $1`"
    if [ -z "$SEARCH_CONTENT" ]; then
        echo "No match found"
    else
        grep -v "$SEARCH_CONTENT" addresses.txt > example.txt.tmp;mv example.txt.tmp addresses.txt
    fi
    head addresses.txt
}
edit(){
    OLD_USER_INFO="$2 $3 $4 $5"
    if [ $1 -eq 1 ]; then
        echo "Enter new First Name:"
        read FNAME
        NEW_USER_INFO="$FNAME $3 $4 $5"
        sed -i "/$OLD_USER_INFO/c $NEW_USER_INFO" addresses.txt 
    elif [ $1 -eq 2 ]; then
        echo "Enter new Last Name:"
        read LNAME
        NEW_USER_INFO="$2 $LNAME $4 $5"
        sed -i "/$OLD_USER_INFO/c $NEW_USER_INFO" addresses.txt 
    elif [ $1 -eq 3 ]; then
        echo "Enter new Address:"
        read ADDR
        NEW_USER_INFO="$2 $3 $ADDR $5"
        sed -i "/$OLD_USER_INFO/c $NEW_USER_INFO" addresses.txt 
    elif [ $1 -eq 4 ]; then
        echo "Enter new Phone:"
        read PHN
        NEW_USER_INFO="$2 $3 $4 $PHONE"
        sed -i "/$OLD_USER_INFO/c $NEW_USER_INFO" addresses.txt 
    else
        echo "Invalid Choice"
    fi
}

edit_user(){
    SEARCH_CONTENT="`cat addresses.txt | grep $1`"
    if [ -z "$SEARCH_CONTENT" ]; then
        echo "No match found"
    else
        echo "----------Edit Options--------------"
        echo "1 edit First Name"
        echo "2 edit Last Name"
        echo "3 edit Address"
        echo "4 edit Phone"
        read EDIT_CHOICE
        edit $EDIT_CHOICE $SEARCH_CONTENT   
    fi
}

menu(){
    while :
        do
            echo "----------------Menu-------------"
            echo "1 for Register"
            echo "2 for View"
            echo "3 for Edit"
            echo "4 for Remove"
            echo "5 for Exit"
            echo "Enter your choice:"
            read CHOICE
            case $CHOICE in
                1)
                    register
                    ;;
                2)
                    echo "Enter search parameter:"
                    read SEARCH
                    view $SEARCH
                    ;;
                3)
                    echo "Enter username to edit:"
                    read SEARCH_EDIT
                    edit_user $SEARCH_EDIT
                    ;;
                4)
                    head addresses.txt
                    echo "Enter username to remove:"
                    read SEARCH_REM
                    remove $SEARCH_REM
                    ;;
                5) 
                    exit 1
                    ;;
                *) 
                    echo "Invalid Choice"
                    ;;
            esac
        done
}
menu